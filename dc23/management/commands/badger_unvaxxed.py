# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from register.models import Attendee


SUBJECT = "[DebConf 23]: Registration Incomplete: Conference COVID-19 vaccination policy"

TEMPLATE = """\
Dear {{ name }},

We notice that your conference registration indicates that you are not
currently fully vaccinated against COVID-19. The conference vaccination policy
[1] requires either vaccination or regular testing throughout the conference.
Since this policy was updated since your registration we require you to confirm
your agreement with this policy to complete your registration.

If you are willing to be regularly tested, you can attend the conference
without a full COVID-19 vaccination. Please update your registration [2] to
indicate that you plan to test regularly.

{% if registered %}If not, please cancel [3] your registration.

{% endif %}Thank you,

The DebConf Registration Team

[1]: https://debconf23.debconf.org/about/covid19/
[2]: https://debconf23.debconf.org/register/step-3
[3]: https://debconf23.debconf.org/register/unregister
"""


class Command(BaseCommand):
    help = 'Unregister the unvaccinated'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Act on a specific user, only'),

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        registered = attendee.user.userprofile.is_registered()
        to = attendee.user.email

        ctx = {
            'name': name,
            'registered': registered,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()
        attendee.completed_register_steps = 3
        attendee.save()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = Attendee.objects.filter(
            completed_register_steps__gt=3,
            confirm_covid_tests__isnull=True,
            vaccinated=False)
        if options['username']:
            queryset = queryset.filter(username=options['username'])

        for attendee in queryset:
            self.badger(attendee, dry_run)
