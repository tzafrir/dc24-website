---
name: About DebConf
---
# About DebConf

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. The last DebConf, [DebConf22][], took
place in Prizren, Kosovo and was attended by 214 participants from over 44
countries.

[DebConf22]: https://debconf22.debconf.org/

**DebConf24 is taking place in Haifa, ISrael from September 10 to September 17,
2023.**

It is being preceded by DebCamp, from September 03 to September 09, 2023.

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

<br />
## Venue

[Technion](https://www.technion.ac.il/en/) is a 

More on the venue and how to get there.

[Accommodation](../accommodation/) is arranged in near-by dorms.

<br />
## Codes of Conduct and Community Team

Please see the [Code of Conduct](../coc/) page for more information.

We look forward to seeing you in Kochi!
