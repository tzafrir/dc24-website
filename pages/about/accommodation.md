---
name: Accommodation
---

# Accommodation

[Technion Eastern dorms](https://www.openstreetmap.org/way/193564904), several
minutes walk from the venue.

Attendees can avail accommodation [bursary](https://debconf24.debconf.org/about/bursaries/).

Fill in on rooms.

Fill in on alternatives.
